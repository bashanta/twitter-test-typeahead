# Publisher Platform Coding Exercise - Typeahead

Thank you for taking the time to interview with Twitter. We have a coding exercise here that
we would like you to work on.

## Note
* The problem was designed to be solvable in an afternoon, yet a robust and comprehensive solution could take much longer. Rather than building a complete solution, please constrain yourself to around three hours and capture any edge cases you identified but did not solve in `considerations.txt`.
* All libraries necessary are already included. You should write every line of code for this submission.
* We love feedback. If there's anything you think could be improved with this exercise, please share your thoughts in `considerations.txt`.

## The task
1) The current code was written to display typeahead results from a hardcoded dataset, but requirements for the feature have changed. You now need to modify the client to communicate with a server so typeahead results can be shared between many users and/or multiple browser windows. Implement two endpoints on the provided server:
	
	a) One to add search terms to the server’s dataset when the search button is pressed and the input is not empty
	b) One to return typeahead results that match the input text (case-insensitive), in descending order of the total number of times the result has been previously searched. This total is the sum of the frequency of all variants of a given typeahead result. A result can have multiple variants, which are exact, case-insensitive matches (e.g. `#GreenNewDeal` and `#greennewdeal`). In the case of more than one variant, only the most frequently searched variant should be returned.

	Then modify the client to utilize the endpoints instead of storing data locally. _Note: This only needs to work on new browsers, so it’s fine to use the Fetch API._

2) To enable better user interaction and accessibility with the typeahead search field, change the background to `#55acee` and the text color to `#fff` when a user hovers over a result. Additionally, make it possible for users to arrow up and down through the results when focus is on the input field, and highlight the selected result with the same style as on hover. Highlighted typeahead results should autofill the search box.

## Setup
This project requires installations of node and yarn.

Instructions for setting up yarn can be found here: https://yarnpkg.com/en/docs/install

Instructions for setting up node can be found here: https://nodejs.org/en/download/

## Available Scripts
The below scripts are to be executed in the `typeahead` directory.

### `yarn start`
Runs the client in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `yarn test`
Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `node src/server`
Runs the server on port 3001.
You will need to restart the server to capture any modifications you make.
(optional) To enable hot-reloading, you can install the `nodemon` tool:

### `npm install nodemon -g`

and start the server with:

### `nodemon src/server`
