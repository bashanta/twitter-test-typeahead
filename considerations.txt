UI considerations
##################
1. Extensive test cases for the multiple flows(eg: onKeyDown handler test hasn't been written in Typeahead component).
   I added few more packages for the testing purpose.
2. when(once a suggestion is clicked) the focus is on the searched result, up/down arrow doesn't do anything
3. Upon clicking search, autocomplete(search result) doesn't disappear. This UX can be improved.
4. UI can be prettier
5. Proper error handling(what happens when something goes wrong on server side etc.)
6. We can improve UX by adding a spinner(by adding fetching in state) if the network(backend) is responding slow
7. Validation on user input before sending it to server(eg: only blank spaces)
8. Upon clicking the searched item, it would be good to trigger the search(as well as save call to the backend)
9. API url is hardcoded to localhost:3001, should be moved to env variables and we need to maintain multiple env files.
   Ideally, the backend url would be proxied.
10. Proper folder structure and I would use redux to manage states in real world applications.
11. In the UI, if backend returns too many records, we need to think about scrollbar or limiting the result.
12. Not a big deal, but we have been using styled component in our project.
13. getRotatingIndex is basically rotating among all items. We should also have a some sort of scrollbar if the result can't fit in the screen)
13. There are many other edge cases if we want to build full blown autocomplete library.

Backend and System design considerations for scale
#############################
1. While adding new search term in the backend storage, I will create a search logs(eg schema: search_phrase, date_time, location etc)
   This would be needed as we will get concurrent issues with current solution if we were to release this solution to production.
2. To build a solution quickly, I have used file and I am doing I/O synchronously(in node application, we should do these things asynchronously not to
   block other requests/users). This will have to be moved to the database and scalable design as opposed to a file storage. Regardless, async is a must.
3. The batch processor I have built is very simple and doesn't handle the scenario if the data can't be fit into memory. This solution ignores the fact that
   previous data was already batch processed. Batch processor can be broken into two main parts(one aggregation, another sorting). In aggregation service, I
   would aggregate the data and store the result with frequency(eg: search_phrase, case_insensitive_total_count, count). Then the second service(sorting)
   can perform further operation to get to the final result. We want to make sure only new records are processed and being aggregated with old data(if it's relevant).
4. Then this can be further used to build a Trie for our better performance. Each node in a Trie can contain 10 results that we can show as user starts typing.
5. The InMemory cache implemented in the example is very simple. In scale, we will need to think about distributed cache using something like Redis and zookeeper.
   While caching these records, we can cache 20% of the frequently used terms. For cache eviction policy, we can use something like LRU policy.
6. Time and location can be used to show more relevant content in the autocomplete.
7. In order to scale this to many users all over the world(say scale in terms of twitter search), we will have some sort of zookeeper to find the server where it would be stored.
   (eg: let us suppose we are only focusing on 65 characters(26 lower case letters, 26 upper case letters, 10 numbers, -, _ and #). Initially, I will start off with few machines,
   then we can come up with different strategy to scale it horizontally. Eg: any letters starting with certain characters would go in one machine. That means, we will need 65 machines
   and to further scale out, we can go to combination of first two(65 * 65 = 4225 machines).
8. However, if we shard based on word, some keyword can become popular(starting with s/a etc). Therefore, we can use consistent hashing as a mechanism to achieve uniform distribution of the data.
9. No tests have been written for backend. I would write extensive unit tests for the backend.
12. Variant filtering is done using hashSet before sending it to UI. The assumption is we wouldn't have a large data set while sending the results(because of limit, pagination etc).
13. Batch processing runs in every 5 seconds, therefore you wouldn't see new entry in the results right away.
15. The results haven't been limited to any number of records. Ideally, we should restrict the number of returned results(most relevant).
16. Error scenarios haven't been handled(while reading from file, while communicating between services etc)
17. I would use logger as opposed to console.log for logging purpose in prod apps.
18. There are other things to consider(eg: fault tolerance, time/location based load)

Suggestions
######################
1. Requirement 1b) is vague, I am not sure if it was done intentionally and wanted candidate to make an assumption/clarify. But, it would be better to break this big requirement into smaller pieces with some examples.
2. Some packages seems to be old(eg: react(to use hooks, you need 16.8.0), react-scripts version(2.1.3)). I think it would be better use the latest version.


