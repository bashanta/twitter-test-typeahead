export const getRotatingIndex = (currentIndex, maxCount, isIncreasing) => {
    if (isIncreasing) {
        currentIndex++;
    } else {
        currentIndex--;
    }

    if (currentIndex < 0) {
        currentIndex = maxCount - 1;
    }

    if (currentIndex === maxCount) {
        currentIndex = 0;
    }

    return currentIndex;
};
