class BatchProcessor {
    constructor(rawHandler, processHandler) {
        this.rawHandler = rawHandler;
        this.processHandler = processHandler;
    }

    execute() {
        const {lowerCaseDictionary, caseSensitiveDictionary} = this.buildDictionaries();
        const sortedData = Object.keys(caseSensitiveDictionary)
            .sort((a, b) => {
                return caseSensitiveDictionary[b] - caseSensitiveDictionary[a];
            }).sort((a, b) => {
                return lowerCaseDictionary[b.toLowerCase()] - lowerCaseDictionary[a.toLowerCase()];
            });
        const content = this.filterDuplicates(sortedData);
        this.processHandler.write(JSON.stringify(content));
    }

    buildDictionaries() {
        const lowerCaseDictionary = {}, caseSensitiveDictionary = {};
        const rawDataSet = this.rawHandler.read().split('\n');
        rawDataSet.forEach((line) => {
            if(line.length == 0) return;
            const lowerCaseLine = line.toLowerCase();
            incrementOrDefault(line, caseSensitiveDictionary);
            incrementOrDefault(lowerCaseLine, lowerCaseDictionary);
        });
        return {lowerCaseDictionary, caseSensitiveDictionary};
    }

    filterDuplicates(words) {
        const visited = new Set();
        return words.filter((key) => {
            const lowerCaseKey = key.toLowerCase();
            if (visited.has(lowerCaseKey)) {
                return false;
            } else {
                visited.add(lowerCaseKey);
                return true;
            }
        });
    };
}

const incrementOrDefault = (key, hash) => {
    if (hash[key]) {
        hash[key]++
    } else {
        hash[key] = 1;
    }
};

module.exports = {
    BatchProcessor: BatchProcessor
};
