class DataApi {
    constructor(cache, rawHandler, processedHandler) {
        this.cache = cache;
        this.rawHandler = rawHandler;
        this.processedHandler = processedHandler
    }

    search(keyword) {
        const cachedData = this.cache.read([keyword]);
        if (cachedData) {
            return cachedData;
        }
        console.log(`No results found in cache for ${keyword}`);
        return this._searchInPersistentStorage(keyword);
    }

    save(keyword) {
        this.rawHandler.append(keyword);
    }

    _searchInPersistentStorage(keyword) {
        const processedData = this.processedHandler.read();
        const parsedData = JSON.parse(processedData);
        const results = parsedData.filter((result) => {
            return keyword.length && result.indexOf(keyword) !== -1;
        });
        this.cache.write(keyword, results);
        console.log(`results count for ${keyword} `, results.length);
        return results;
    }
}

module.exports = {
    DataApi: DataApi
};
