const fs = require('fs');
const rawFileName = __dirname + '/files/raw_data.txt';
const processedFileName = __dirname + '/files/processed_data.txt';

const fileHandle = (fileName) => {
    return {
        write: (content) => {
            console.log(`writing into ${fileName}`);
            fs.writeFileSync(fileName, content);
        },
        read: () => {
            console.log(`reading from ${fileName}`);
            return fs.readFileSync(fileName, 'utf8');
        },
        append: (searchTerm) => {
            fs.appendFile(fileName, `${searchTerm}\n`, 'utf8', (err) => {
                if (err) throw err;
                console.log(`appending the ${searchTerm} to ${fileName}`);
            });
        },
    }
};

module.exports = {
    rawHandler: fileHandle(rawFileName),
    processedHandler: fileHandle(processedFileName),
};
