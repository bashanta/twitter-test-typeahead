// InMemory Cache
class Cache {
    constructor() {
        this.store = {};
    }

    read(key) {
        console.log('reading data from cache for key', key);
        return this.store[key];
    }

    write(key , value) {
        console.log('writing data into cache for key', key);
        this.store[key] = value;
    }

    flush() {
        console.log('Flushing cache data');
        this.store = {};
    }
}

module.exports = {
    Cache: Cache,
};
