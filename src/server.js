const express = require('express');
const {rawHandler, processedHandler} = require('./server/storage_handler');
const { Cache } = require('./server/cache');
const {DataApi} = require('./server/data_api');
const {BatchProcessor} = require('./server/batch_processor');

const app = express();
const port = 3001;
const cache = new Cache();
const dataApi = new DataApi(cache, rawHandler, processedHandler);
const batchProcessor = new BatchProcessor(rawHandler, processedHandler);

setInterval(() => {
    console.log("====> Started batch processing at ", new Date());
    batchProcessor.execute();
    cache.flush();
    console.log("====> Ending batch processing at", new Date());
}, 5000);

app.get('/typeahead', (req, res) => {
    const keyword = req.query.keyword;
    console.log('/typeahead request for keyword: ', keyword);
    const typeaheadResults = dataApi.search(keyword); // this will be async action in real world example.
    res.header("Access-Control-Allow-Origin", "*");
    res.send(typeaheadResults);
});

app.get('/add', (req, res) => {
    const keyword = req.query.keyword;
    console.log('/add request for keyword: ', keyword);
    dataApi.save(keyword);
    res.header("Access-Control-Allow-Origin", "*");
    res.send({});
});

app.listen(port, () => console.log(`Typeahead server listening on port ${port}!`))