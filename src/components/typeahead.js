import React, {Component} from 'react';
import './typeahead.css';
import SearchResult from './searchresult';
import typeaheadApi from './typeaheadApi';
import {getRotatingIndex} from '../helper';

class Typeahead extends Component {
    state = {
        typeaheadResults: [],
        activeIndex: -1,
        searchedItem: '',
        errorSearchApi: false,
    };

    render() {
        const {typeaheadResults, activeIndex, searchedItem} = this.state;
        return (
            <div className="container">
                <div>Search Hashtags</div>
                <div className="searchContainer">
                    <input className="searchBox" type="search" id="typeahead" value={searchedItem}
                           onChange={this._handleSearchInput} onKeyDown={this._handleKeyPress}/>
                    <button onClick={this._handleSubmit} className='searchButton'>
                        Search
                    </button>
                </div>
                {
                    typeaheadResults &&
                    <SearchResult
                        highlightItem={this._highlightSelectedItem}
                        typeaheadResults={typeaheadResults}
                        activeIndex={activeIndex}/>
                }

            </div>
        );
    }

    _handleSubmit = () => {
        if (this.state.searchedItem.length > 0) {
            typeaheadApi.addSearchedItem(this.state.searchedItem);
        }
    };

    _highlightSelectedItem = (index) => {
        const selectedItem = this.state.typeaheadResults[index];
        this.setState({searchedItem: selectedItem, activeIndex: index});
    };

    _handleUpDown = (isIncreasing) => {
        const {typeaheadResults, activeIndex} = this.state;
        const totalResult = typeaheadResults.length;
        if (totalResult > 0) {
            const newIndex = getRotatingIndex(activeIndex, totalResult, isIncreasing);
            this._highlightSelectedItem(newIndex);
        }
    };

    _handleKeyPress = async (event) => {
        const {key} = event;
        if ('ArrowDown' === key) {
            event.preventDefault();
            this._handleUpDown(true);
        } else if (key === 'ArrowUp') {
            event.preventDefault();
            this._handleUpDown(true);
        } else if (key === 'Enter') {
            this._handleSubmit();
        } else {
            this.setState({activeIndex: -1});
        }
    };

    _handleSearchInput = async (e) => {
        const {value} = e.target;

        try {
            const typeaheadResults = await typeaheadApi.fetchItems(value);
            this.setState({typeaheadResults, errorSearchApi: false, searchedItem: value});
        } catch {
            this.setState({errorSearchApi: true, searchedItem: value});
        }
    };
}

export default Typeahead;
