import React from 'react';
import PropTypes from 'prop-types';

const SearchResult = ({activeIndex, typeaheadResults, highlightItem}) => (
    <div id="typeahead-results" className="typeaheadResults">
        {
            typeaheadResults.map((item, index) => renderItem(item, index, activeIndex, highlightItem))
        }
    </div>
);

const renderItem = (item, index, activeIndex, highlightItem) => {
    const activeClass = index === activeIndex ? 'active' : '';
    return (
        <div key={item}
             className={`typeaheadItem ${activeClass}`}
             onMouseOver={() => highlightItem(index)}>
            {item}
        </div>
    )
};

SearchResult.propTypes = {
    activeIndex: PropTypes.number.isRequired,
    typeaheadResults: PropTypes.arrayOf(PropTypes.string).isRequired,
    highlightItem: PropTypes.func.isRequired,
};

export default SearchResult;