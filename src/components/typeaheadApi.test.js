import typeaheadApi from "./typeaheadApi";

describe('typeaheadApi', () => {
    beforeEach(() => {
        global.fetch.resetMocks();
        jest.resetAllMocks();
    });

    describe('fetchItems', async () => {
        let results;

        beforeEach( async () => {
            global.fetch.mockResponse(JSON.stringify(['Twitter','Twelve']), {status: 200});
            results = await typeaheadApi.fetchItems('Tw');
        });

        it('calls typeahead API to get the results', () => {
            expect(global.fetch).toHaveBeenCalledWith('http://localhost:3001/typeahead?keyword=Tw');
        });

        it('returns the matching results', () =>  {
            expect(results).toMatchObject(['Twitter','Twelve']);
        });

        it('throws an error when something goes wrong in promise handle', async () => {
            global.fetch.mockResponse('', {status: 200});
            await expect(typeaheadApi.fetchItems('Astrophy')).rejects.toThrow('invalid json response body at undefined reason: Unexpected end of JSON input');
        });
    });

    describe('addSearchedItem', () => {
        it('calls typeahead add API to save the record', function () {
            typeaheadApi.addSearchedItem('Goo');
            expect(global.fetch).toHaveBeenCalledWith('http://localhost:3001/add?keyword=Goo');
        });
    });
});
