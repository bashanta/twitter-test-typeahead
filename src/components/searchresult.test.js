import React from 'react';
import {shallow} from 'enzyme';
import SearchResult from './searchresult';

describe('SearchResult', () => {
    const highlightItemMock = jest.fn();
    const results = ['Item at index 0', 'Item at index 1'];
    let props;

    beforeEach(() => {
        props = {
            typeaheadResults: results,
            highlightItem: highlightItemMock,
            activeIndex: -1
        };
    });

    it('renders the search result with all the items', () => {
        let wrapper = shallow(<SearchResult {...props} />);
        expect(wrapper.find('.typeaheadResults')).toExist();
        expect(wrapper.find('.typeaheadItem').length).toBe(2);
    });

    describe('when activeIndex matches of the index of items', () => {
        it('displays the active class to that item', () => {
            let wrapper = shallow(<SearchResult {...props} activeIndex={1}/>);
            expect(wrapper.find('.typeaheadItem.active').text()).toBe('Item at index 1');
        });
    });

    describe('onMouseOver', () => {
        it('triggers a prop call to highlight the item', () => {
            let wrapper = shallow(<SearchResult {...props} />);
            wrapper.find('.typeaheadItem').first().simulate('mouseover');
            expect(highlightItemMock).toHaveBeenCalledWith(0);
        });
    });
});
