import React from 'react';
import {shallow} from 'enzyme';
import Typeahead from './typeahead';
import SearchResult from './searchresult';
import typeaheadApi from "./typeaheadApi";

jest.mock('./typeaheadApi');

describe('Typeahead', () => {
    const results = ['Twitter', 'Test'];
    let wrapper;

    beforeEach(() => {
        jest.resetAllMocks();
        wrapper = shallow(<Typeahead/>);
    });

    describe('By default', () => {
        it('renders the search result with all the items', () => {
            expect(wrapper.find('.searchContainer')).toExist();
            expect(wrapper.find('.searchBox')).toExist();
        });

        it('renders SearchResult component with default props', () => {
            const resultComponent = wrapper.find(SearchResult);
            expect(resultComponent).toHaveProp('highlightItem', wrapper.instance()._highlightSelectedItem);
            expect(resultComponent).toHaveProp('typeaheadResults', []);
            expect(resultComponent).toHaveProp('activeIndex', -1);
        });
    });

    describe('on input change', () => {
        it('triggers a call to fetch items', (done) => {
            jest.spyOn(typeaheadApi, 'fetchItems').mockReturnValue([]);
            wrapper.find('.searchBox').simulate('change', {target: {value: 'Test'}});
            setImmediate(() => {
                expect(typeaheadApi.fetchItems).toHaveBeenCalledWith('Test');
                done();
            });
        });

        it('displays SearchResult component with the search results ', (done) => {
            jest.spyOn(typeaheadApi, 'fetchItems').mockReturnValue(results);
            wrapper.find('.searchBox').simulate('change', {target: {value: 'T'}});
            setImmediate(() => {
                expect(wrapper.find(SearchResult)).toHaveProp('typeaheadResults', results);
                done();
            });
        });

        it('sets errorSearchApi to true if api call fails', (done) => {
            jest.spyOn(typeaheadApi, 'fetchItems').mockReturnValue(Promise.reject('bad request'));
            wrapper.find('.searchBox').simulate('change', {target: {value: 'T'}});
            setImmediate(() => {
                expect(wrapper.instance().state.errorSearchApi).toBe(true);
                done();
            });
        });
    });

    describe('upon clicking search button', () => {

        beforeEach(() => {
            jest.spyOn(typeaheadApi, 'addSearchedItem');
        });

        describe('when the search field has data', () => {
            it('triggers a call to save the data', () => {
                wrapper.setState({searchedItem: 'Tr'});
                wrapper.find('.searchButton').simulate('click');
                expect(typeaheadApi.addSearchedItem).toHaveBeenCalledWith('Tr');
            });
        });

        describe('when the search field is blank', () => {
            it('does not trigger a call to save the data', () => {
                wrapper.find('.searchBox').simulate('change', {target: {value: ''}});
                wrapper.find('.searchButton').simulate('click');
                expect(typeaheadApi.addSearchedItem).not.toHaveBeenCalled();
            });
        });
    });
});
