const API_BASE_URL = 'http://localhost:3001';

const typeaheadApi = {
    fetchItems: (keyword) => {
        const encodedValue = encodeURIComponent(keyword);
        return fetch(`${API_BASE_URL}/typeahead?keyword=${encodedValue}`)
            .then(res => res.json());
    },

    addSearchedItem: (keyword) => {
        const encodedValue = encodeURIComponent(keyword);
        return fetch(`${API_BASE_URL}/add?keyword=${encodedValue}`);
    }
};

export default typeaheadApi;
