import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Typeahead from './components/typeahead';

ReactDOM.render(<Typeahead/>, document.getElementById('root'));
